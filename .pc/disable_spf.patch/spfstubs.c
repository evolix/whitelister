#include <caml/mlvalues.h>
#include <caml/memory.h>

#include <spf.h>

value spf_query(value from, value ip, value helo)
{
    int res;
    peer_info_t* peer_info;
    
    CAMLparam3(from, ip, helo);

    peer_info = SPF_init("whitelister", String_val(ip), NULL, NULL, NULL, 0, 0);
    SPF_smtp_helo(peer_info, String_val(helo));
    SPF_smtp_from(peer_info, String_val(from));
    res = peer_info->RES = SPF_policy_main(peer_info);

    SPF_close(peer_info);

    switch(res)
    {
        case SPF_PASS:
            CAMLreturn(Val_int(0));

        case SPF_NEUTRAL:
            CAMLreturn(Val_int(1));

        case SPF_NONE:
            CAMLreturn(Val_int(2));

        case SPF_S_FAIL:
            CAMLreturn(Val_int(3));

        case SPF_H_FAIL:
            CAMLreturn(Val_int(4));

        default: /* SPF_ERROR, SPF_UNKNOWN, SPF_UNMECH */
            CAMLreturn(Val_int(5));
            
    }
}

