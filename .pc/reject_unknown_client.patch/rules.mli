(**************************************************************************)
(*                                                                        *)
(*      whitelister : a Whitelister Policy Daemon for Postfix             *)
(*      ~~~~~~~~~~~                                                       *)
(*                                                                        *)
(*  Copyright (C) 2005 AAEGE.org                                          *)
(*  Author : Pierre Habouzit <pierre.habouzit@m4x.org>                    *)
(*  ____________________________________________________________________  *)
(*                                                                        *)
(*  This program is free software; you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation; either version 2 of the License, or     *)
(*  (at your option) any later version.                                   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program; if not, write to the Free Software           *)
(*  Foundation, Inc.,                                                     *)
(*  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA               *)
(*                                                                        *)
(**************************************************************************)

exception Dirty of string
exception Reject of string
type spf_mode = Spf_off | Spf_normal | Spf_strict | Spf_paranoid
type rhbl_type = Helo | Rcpt | Sender | Client

(**
 * this module defines some rules that a Policy should verify to be whitelisted.
 *
 * quoting POSTFIX POLICY README [1] :
 *     In case of trouble the policy server must not send a reply.
 *     Instead the server must log a warning and disconnect.
 *     Postfix will retry the request at some later time.
 *
 * We DON'T follow that policy since we write a WHITELIST server.
 * So our choice is that any problem is similar to a invalid Check.
 *
 * [1] http://www.postfix.org/SMTPD_POLICY_README.html
 *)
  
val check_rbl : string list -> Policy.t -> unit

val check_rhbl : rhbl_type -> string list -> Policy.t -> unit

val check_spf : spf_mode -> bool -> Policy.t -> unit
