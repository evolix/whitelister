##########################################################################
#                                                                        #
#      whitelister : a Whitelister Policy Daemon for Postfix             #
#      ~~~~~~~~~~~                                                       #
#                                                                        #
#  Copyright (C) 2005 AAEGE.org                                          #
#  Author : Pierre Habouzit <pierre.habouzit@m4x.org>                    #
#  ____________________________________________________________________  #
#                                                                        #
#  This program is free software; you can redistribute it and/or modify  #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation; either version 2 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  This program is distributed in the hope that it will be useful,       #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#                                                                        #
#  You should have received a copy of the GNU General Public License     #
#  along with this program; if not, write to the Free Software           #
#  Foundation, Inc.,                                                     #
#  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA               #
#                                                                        #
##########################################################################

VERSION = 0.8
PROGRAM = whitelister
PKG_DIST = $(PROGRAM)-$(VERSION)
REPO = $(shell svn info | grep URL | sed -e 's,URL *: *,,')

PKGS   = -package syslog
CFLAGS = -O2 -Wall -fPIC
CCLIB  = -cclib
CCOPT  = $(foreach opt,$(CFLAGS),-ccopt $(opt))

OCAMLC  = ocamlfind ocamlc   $(PKGS) $(CCOPT) $(CCLIB)
OCAMLOPT= ocamlfind ocamlopt -linkpkg $(PKGS) $(CCOPT) $(CCLIB)
OCAMLDEP= ocamlfind ocamldep $(PKGS)

BIB = str.cmxa unix.cmxa syslog.cmxa
CMX = policy.cmx rules.cmx server.cmx whitelister.cmx

##############################################################

all: $(PROGRAM)

whitelister: $(CMX)
	ocamlfind ocamlopt $(PKGS) $(CCOPT) -o $@ $(BIB) $^

headers: Makefile *.ml *.mli
	headache -h tpl/header $^

dist:
	@rm -rf $(PKG_DIST) $(PKG_DIST).tar.gz
	@svn export $(REPO) $(PKG_DIST)
	@rm -rf $(PKG_DIST)/debian
	@tar czf $(PKG_DIST).tar.gz $(PKG_DIST)
	@rm -rf $(PKG_DIST)
	@echo -e "\ndistribution built in $(PKG_DIST).tar.gz\n"


##############################################################

.SUFFIXES: .mli .ml .cmi .cmo .cmx .mll .mly .c .o

.c.o:
	$(OCAMLC) $(CCOPT) -c $<

.mli.cmi:
	$(OCAMLC) -c $<

.ml.cmo:
	$(OCAMLC) -c $<

.ml.cmx:
	$(OCAMLOPT) -a str.cmxa -c $<

.mll.ml:
	$(OCAMLLEX) $<

##############################################################
cleanbyte:
	rm -rf *.{cm?,o} *~

clean: cleanbyte
	rm -f .depend
	rm -f $(PROGRAM)

.depend depend: *.ml *.mli
	rm -f .depend
	$(OCAMLDEP) *.ml *.mli > .depend

include .depend
